# re-frame-throttle

An effect to throttle Re-frame event dispatching. For the differences
between debouncing and throttling, see [this handy visualisation](http://demo.nimius.net/debounce_throttle/).

## Usage

1. Add dependency to your project. See [Clojars for details](https://clojars.org/org.clojars.salohonka/re-frame-throttle).

2. Require the namespace:
```clj
(ns app.core
  (:require
    ...
    [re-frame-throttle.core]
    ...))
```

3. Use the effect in an event:
```clj
(rf/reg-event-fx :foo/throttled-bar
 (fn [_]
   {:throttle-dispatch
     {:event [:bar "baz"]
      :ms    300          ; Defaults to 500 ms.
      :id    :foo/bar}}))
```

## Details

`:throttle-dispatch` has three keys, two of which are required:
* `:event` is a required key. The value is dispatched in a throttled manner.
* `:id` is a required key. The value is used to determine which events are
  throttled together. Events sharing the same id are throttled together.
* `:ms` is an optional key. The value is the amount of milliseconds waited
  between dispatching the latest value of `:event`. If the key is not
  specified, the amount defaults to 500 ms.

## License

Copyright © 2021 Matias Salohonka

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.
