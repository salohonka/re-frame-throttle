(defproject org.clojars.salohonka/re-frame-throttle "1.0.1"
  :description "Throttling effect for Re-frame events."
  :url "https://gitlab.com/salohonka/re-frame-throttle.git"
  :license {:name "EPL-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [org.clojure/clojurescript "1.10.773"]
                 [re-frame "1.1.2"]]
  :source-paths ["src"]
  :scm {:name "git" :url "https://gitlab.com/salohonka/re-frame-throttle"})
