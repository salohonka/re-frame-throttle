(ns re-frame-throttle.core
  (:require [re-frame.core :as rf]
            [re-frame.db :as db]))

(defn- dissoc-in [m ks]
  (if-let [[k & ks] (seq ks)]
    (if (seq ks)
      (let [v (dissoc-in (get m k) ks)]
        (if (empty? v)
          (dissoc m k)
          (assoc m k v)))
      (dissoc m k))
    m))

(rf/reg-fx :throttle-dispatch
  (fn [{:keys [ms event id] :or {ms 500}}]
    (assert event "Re-frame truthy event must be present in :event key.")
    (assert id "An unique truthy identifier for throttling must be present in :id key.")
    (assert (number? ms) "The throttle duration must be a number.")
    (let [callback (fn []
                     (let [db @db/app-db
                           event (get-in db [:throttle/events id])]
                       (if event
                         (do (rf/dispatch event)
                             (swap! db/app-db dissoc-in [:throttle/events id]))
                         (do (js/clearInterval (get-in db [:throttle/intervals id]))
                             (swap! db/app-db dissoc-in [:throttle/intervals id])))))]
      (swap! db/app-db assoc-in [:throttle/events id] event)
      (when-not (get-in @db/app-db [:throttle/intervals id])
        (swap! db/app-db assoc-in [:throttle/intervals id] (js/setInterval callback ms))))))
